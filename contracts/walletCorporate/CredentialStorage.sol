// SPDX-License-Identifier: None
pragma solidity ^0.8.0;

import "./CompanyManagement.sol";

contract CredentialStorage is CompanyManagement{
    struct Credential {
        string credentialId;
        string url; // URL where the credential data can be accessed.
        address[] authorizedUsers; // List of user addresses authorized to access the credential.
        string[] authorizedGroups; // List of groups authorized to access the credential.
    }

    mapping(string => Credential) public credentials; // Maps credential IDs to Credential struct instances.

    /**
     * @dev Internal function to add a new credential.
     * @param credentialId memory string: The unique identifier for the credential.
     * @param url memory string: The URL associated with the credential.
     */
    function _addCredential(string memory credentialId, string memory url) internal {
        require(!isExist(credentialId), "Credential already exists");

        address[] memory newAuthUsers = new address[] (1);
        newAuthUsers[0] = msg.sender;
        string[] memory newAuthGroup;

        credentials[credentialId] = Credential({
            credentialId: credentialId,
            url: url,
            authorizedUsers: newAuthUsers,
            authorizedGroups: newAuthGroup
        });
    }

    /**
     * @dev Retrieves the URL for a given credential.
     * @param credentialId memory string: The ID of the credential.
     * @return string memory: The URL associated with the credential.
     */
    function credentialIdToURL(string memory credentialId) public view returns (string memory) {
        return credentials[credentialId].url;
    }

    /**
     * @dev Checks if a credential exists.
     * @param credentialId memory string: The ID of the credential.
     * @return bool: True if the credential exists, false otherwise.
     */
    function isExist(string memory credentialId) public view returns (bool) {
        return bytes(credentials[credentialId].url).length > 0;
    }

    /**
     * @dev Adds users to the authorized users list for a specific credential.
     * @param credentialId memory string: The ID of the credential.
     * @param users memory address[]: Array of user addresses to be authorized.
     */
    function addAuthorizedUsers(string memory credentialId, address[] memory users) public {
        require(isExist(credentialId), "Credential does not exist");
        Credential storage credential = credentials[credentialId];
        for (uint i = 0; i < users.length; i++) {
            credential.authorizedUsers.push(users[i]);
        }
    }

    /**
     * @dev Removes users from the authorized users list for a specific credential.
     * @param credentialId memory string: The ID of the credential.
     * @param users memory address[]: Array of user addresses to be removed from authorization.
     */
    function removeAuthorizedUsers(string memory credentialId, address[] memory users) public {
        require(isExist(credentialId), "Credential does not exist");
        Credential storage credential = credentials[credentialId];
        for (uint j = 0; j < users.length; j++) {
            address user = users[j];
            uint index = credential.authorizedUsers.length;
            for (uint i = 0; i < credential.authorizedUsers.length; i++) {
                if (credential.authorizedUsers[i] == user) {
                    index = i;
                    break;
                }
            }
            if (index < credential.authorizedUsers.length) {
                credential.authorizedUsers[index] = credential.authorizedUsers[credential.authorizedUsers.length - 1];
                credential.authorizedUsers.pop();
            }
        }
    }

    /**
     * @dev Adds groups to the authorized groups list for a specific credential.
     * @param credentialId memory string: The ID of the credential.
     * @param groups memory string[]: Array of groups names to be authorized.
     */
    function addAuthorizedGroups(string memory credentialId, string[] memory groups) public {
        require(isExist(credentialId), "Credential does not exist");
        Credential storage credential = credentials[credentialId];
        for (uint i = 0; i < groups.length; i++) {
            credential.authorizedGroups.push(groups[i]);
        }
    }

    /**
     * @dev Removes groups from the authorized groups list for a specific credential.
     * @param credentialId memory string: The ID of the credential.
     * @param groups memory string[]: Array of groups names to be removed from authorization.
     */
    function removeAuthorizedGroups(string memory credentialId, string[] memory groups) public {
        require(isExist(credentialId), "Credential does not exist");
        Credential storage credential = credentials[credentialId];
        for (uint j = 0; j < groups.length; j++) {
            string memory groupName = groups[j];
            uint index = credential.authorizedGroups.length;
            for (uint i = 0; i < credential.authorizedGroups.length; i++) {
                if (keccak256(bytes(credential.authorizedGroups[i])) == keccak256(bytes(groupName))) {
                    index = i;
                    break;
                }
            }
            if (index < credential.authorizedGroups.length) {
                credential.authorizedGroups[index] = credential.authorizedGroups[credential.authorizedGroups.length - 1];
                credential.authorizedGroups.pop();
            }
        }
    }

    /**
     * @dev Checks if a user or a member of an authorized group has access to a credential.
     * @param credentialId memory string: The ID of the credential to check access for.
     * @return bool True if the user has access, either directly or via a group.
     */
    function checkAccess(string memory credentialId) public view returns (bool) {
        Credential memory cred = credentials[credentialId];
        address user = msg.sender;

        for (uint i = 0; i < cred.authorizedUsers.length; i++) {
            if (cred.authorizedUsers[i] == user
                && (userPermissions[cred.authorizedUsers[i]].read
                || userPermissions[cred.authorizedUsers[i]].present
                    || userPermissions[cred.authorizedUsers[i]].consume
                )
            ) {
                return true;
            }
        }

        for (uint i = 0; i < cred.authorizedGroups.length; i++) {
            if (groupMembers[cred.authorizedGroups[i]][user]
            && groupExists[cred.authorizedGroups[i]] == true
                && ( groupPermissions[cred.authorizedGroups[i]].read
                || groupPermissions[cred.authorizedGroups[i]].present
                    || groupPermissions[cred.authorizedGroups[i]].consume
                )
            ) {
                return true;
            }
        }

        return false;
    }

    /**
     * @dev Retrieves authorized user addresses for a specific credential.
     * @param credentialId string memory: The ID of the credential.
     * @return address[] memory: Array of addresses authorized to access the credential.
     */
    function getAuthorizedUsers(string memory credentialId) public view returns (address[] memory) {
        require(isExist(credentialId), "Credential does not exist");
        return credentials[credentialId].authorizedUsers;
    }

    /**
     * @dev Retrieves authorized groups for a specific credential.
     * @param credentialId string memory: The ID of the credential.
     * @return string[] memory: Array of group names authorized to access the credential.
     */
    function getAuthorizedGroups(string memory credentialId) public view returns (string[] memory) {
        require(isExist(credentialId), "Credential does not exist");
        return credentials[credentialId].authorizedGroups;
    }
}