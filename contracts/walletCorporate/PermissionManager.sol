// SPDX-License-Identifier: None
pragma solidity ^0.8.0;

contract PermissionManager {
    struct Permission {
        bool read;
        bool consume;
        bool present;
        bool importC;
        bool walletCorpo;
    }

    mapping(address => Permission) public userPermissions; // Maps each user to their specific permissions
    mapping(string => Permission) public groupPermissions; // Maps each group to their specific permissions
    mapping(string => mapping(address => bool)) public groupMembers; // Maps group names to user addresses to denote membership.
    mapping(string => address[]) public groupMemberList; // Maps group names to lists of member addresses.
    mapping(string => bool) public groupExists; // Maps group names to a boolean indicating whether the group exists.
    string[] public groupNames; //List of all group names.

    /**
    * @dev Adds or updates permissions for an individual user
    * @param user address: The address of the user whose permissions are to be set or updated
    * @param perms Permission memory: The permissions to be granted to the user, encapsulated in a Permission struct
    */
    function setUserPermission(address user, Permission memory perms) public {
        userPermissions[user] = perms;
    }

    /**
    * @dev Removes permissions for an individual user
    * @param user address: The address of the user whose permissions are to be removed
    */
    function removeUserPermission(address user) public {
        delete userPermissions[user];
    }

    /**
    * @dev Adds or updates permissions for a group
    * @param groupName string memory: The name of the group whose permissions are to be set or updated
    * @param perms Permission memory: The permissions to be granted to the group, encapsulated in a Permission struct
    */
    function setGroupPermission(string memory groupName, Permission memory perms) public {
        require(groupExists[groupName], "Group does not exist");
        groupPermissions[groupName] = perms;
    }

    /**
    * @dev Removes permissions for a group
    * @param groupName string memory: The name of the group whose permissions are to be removed
    */
    function removeGroupPermission(string memory groupName) public {
        require(groupExists[groupName], "Group does not exist");
        delete groupPermissions[groupName];
    }

    /**
     * @dev Create a group and its associations.
     * @param groupNamesArray memory string: The name of the group.
     */
    function createGroups(string[] memory groupNamesArray) public {
        for (uint i = 0; i < groupNamesArray.length; i++) {
            string memory groupName = groupNamesArray[i];
            if (!groupExists[groupName]) {
                groupNames.push(groupName);
                groupExists[groupName] = true;
            }
        }
    }

    /**
     * @dev Deletes a group and its associations.
     * @param groupName memory string: The name of the group to delete.
     */
    function deleteGroup(string memory groupName) public {
        require(groupExists[groupName], "Group does not exist");

        groupExists[groupName] = false;

        for (uint i = 0; i < groupNames.length; i++) {
            if (keccak256(bytes(groupNames[i])) == keccak256(bytes(groupName))) {
                groupNames[i] = groupNames[groupNames.length - 1];
                groupNames.pop();
                break;
            }
        }
    }

    /**
     * @dev Adds members to a group.
     * @param groupName memory string: The name of the group.
     * @param members memory address[]: Array of addresses to add as group members.
     */
    function addMembersToGroup(string memory groupName, address[] memory members) public {
        require(groupExists[groupName], "Group does not exist");
        for (uint i = 0; i < members.length; i++) {
            if (!groupMembers[groupName][members[i]]) {
                groupMembers[groupName][members[i]] = true;
                groupMemberList[groupName].push(members[i]);
            }
        }
    }

    /**
     * @dev Removes members from a group.
     * @param groupName memory string: The name of the group.
     * @param members memory address[]: Array of addresses to remove from the group.
     */
    function removeMembersFromGroup(string memory groupName, address[] memory members) public {
        require(groupExists[groupName], "Group does not exist");
        for (uint i = 0; i < members.length; i++) {
            if (groupMembers[groupName][members[i]]) {
                delete groupMembers[groupName][members[i]];
                for (uint j = 0; j < groupMemberList[groupName].length; j++) {
                    if (groupMemberList[groupName][j] == members[i]) {
                        groupMemberList[groupName][j] = groupMemberList[groupName][groupMemberList[groupName].length - 1];
                        groupMemberList[groupName].pop();
                        break;
                    }
                }
            }
        }
    }

    /**
     * @dev Retrieves all members of a specified group.
     * @param groupName memory string: The name of the group.
     * @return address[] memory: Array of member addresses.
     */
    function getMembersOfGroup(string memory groupName) public view returns (address[] memory) {
        require(groupExists[groupName], "Group does not exist");
        return groupMemberList[groupName];
    }

    /**
     * @dev Retrieves all existing group names.
     * @return string[] memory: Array of all group names.
     */
    function getAllGroups() public view returns (string[] memory) {
        return groupNames;
    }
}
